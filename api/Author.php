<?php

class Author
{
    public string $firstName;
    public string $lastName;
    public ?int $id;
    public ?int $rating;

    public function __construct($firstName, $lastName, ?int $id, ?int $rating) {
        $this->firstName = urldecode($firstName);
        $this->lastName = urldecode($lastName);
        $this->id = $id;
        $this->rating = $rating;
    }

    public function getAuthorDataAsCsv(): string {
        return join(";", [$this->id, $this->firstName, $this->lastName]);
    }

    public function toString(): string {
        return urldecode($this->firstName) . " " . urldecode($this->lastName);
    }
 }