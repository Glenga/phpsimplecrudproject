<?php

class Book
{
    public string $title;
    public ?int $grade;
    public ?bool $isRead;
    public ?int $id;

    public ?string $authors;
    public array $authorsArray = [];

    public function __construct(string $title, ?int $grade, ?bool $isRead, ?int $id) {
        $this->title = $title;
        $this->grade = $grade;
        $this->isRead = $isRead;
        $this->id = $id;
    }

    public function addAuthor($author) {
        $this->authorsArray[] = $author;
        $this->authors = $this->getAllAuthorsAsString();
    }

    public function getAllAuthorsAsString(): string {
        $authorAsString = [];
        foreach ($this->authorsArray as $author) {
            $authorAsString[] = $author->toString();
        }
        return join(", ", $authorAsString);
    }

    public static function deserializeBook($data): Book {
        return new Book($data['title'], $data['grade'] ?? null, $data['isRead'] ?? null, $data['id'] ?? null);
    }
}