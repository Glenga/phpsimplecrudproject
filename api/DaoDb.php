<?php

class DaoDb
{
    const USERNAME = 'glenga';
    const PASSWORD = '0805d7';

    private PDO $connection;

    public function __construct() {
        $this->connection = $this->getConnection();
    }

    private function getConnection(): PDO
    {
        $host = 'db.mkalmo.xyz';

        $address = sprintf('mysql:host=%s;port=3306;dbname=%s',
            $host, self::USERNAME);

        return new PDO($address, self::USERNAME, self::PASSWORD);
    }

    public function addBook(Book $book) {
        $stmt = $this->connection->prepare(
            'INSERT INTO book (book_title, rating, have_read) VALUES (:bookTitle, :rating, :isRead)'
        );

        $this->bindBookValues($stmt, $book);
        $stmt->execute();

        $bookId = $this->connection->lastInsertId();
        $this->addDataToBooksAndAuthors($book->authorsArray, $bookId);
    }

    public function updateBook(Book $book) {
        $stmt = $this->connection->prepare(
            'UPDATE book SET book_title = :bookTitle, rating = :rating, have_read = :isRead WHERE id = :id'
        );

        $this->bindBookValues($stmt, $book);
        $stmt->bindValue(':id', $book->id);

        $this->deleteDataFromBooksAndAuthorsByBookId($book->id);
        $this->addDataToBooksAndAuthors($book->authorsArray, $book->id);

        $stmt->execute();
    }

    public function getBookById(string $id): ?Book {
        $stmt = $this->connection->prepare(
            'SELECT a.id as author_id, b.id as book_id, book_title, b.rating, have_read, first_name, last_name FROM book b
               LEFT JOIN books_and_authors ba ON b.id = ba.book_id
               LEFT JOIN author a ON ba.author_id = a.id WHERE b.id = :id'
        );
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        $bookStatements = $stmt->fetchAll();
        if (count($bookStatements) == 0) {
            return null;
        }

        $book = null;
        foreach ($bookStatements as $statement) {
            if (!isset($book)) {
                $book = new Book(urldecode(
                    $statement['book_title']),
                    $statement['rating'],
                    $statement['have_read'],
                    $statement['book_id']);
            }
            if ($statement['first_name'] != null && $statement['last_name'] != null) {
                $author = new Author(
                    $statement['first_name'],
                    $statement['last_name'],
                    $statement['author_id'],
                    null);
                $book->addAuthor($author);
            }
        }
        return $book;
    }

    public function deleteBookById(string $id) {
        $stmt = $this->connection->prepare(
            'DELETE FROM book WHERE id = :bookId'
        );
        $stmt->bindValue(':bookId', $id);
        $this->deleteDataFromBooksAndAuthorsByBookId($id);

        $stmt->execute();
    }

    public function getAllBooks(): array
    {
        $stmt = $this->connection->prepare(
            'SELECT * FROM book'
        );

        $stmt->execute();
        $books = $stmt->fetchAll();

        $booksDtoList = [];
        foreach ($books as $book) {
            $bookDto = new Book(urldecode($book['book_title']), $book['rating'], null, $book['id']);
            $booksDtoList[] = $bookDto;
        }
        return $booksDtoList;
    }

    public function addAuthor(Author $author) {
        $stmt = $this->connection->prepare(
            'INSERT INTO author (first_name, last_name, rating) VALUES (:firstName, :lastName, :rating)'
        );

        $this->bindAuthorValues($stmt, $author);
        $stmt->execute();
    }

    public function updateAuthor(Author $author) {
        $stmt = $this->connection->prepare(
            'UPDATE author SET first_name = :firstName, last_name = :lastName, rating = :rating WHERE id = :id'
        );
        var_dump($author);

        $stmt->bindValue(':id', $author->id);
        $this->bindAuthorValues($stmt, $author);

        var_dump($stmt);

        $stmt->execute();
    }

    public function getAuthorById(string $id): Author {
        $stmt = $this->connection->prepare(
            'SELECT first_name, last_name, rating FROM author WHERE author.id = :authorId'
        );

        $stmt->bindValue(':authorId', $id);
        $stmt->execute();
        $stmt = $stmt->fetch();

        return new Author(
            $stmt['first_name'],
            $stmt['last_name'],
            $id,
            $stmt['rating']
        );
    }

    public function deleteAuthorById(string $id) {
        $stmt = $this->connection->prepare(
            'DELETE FROM author WHERE id = :id'
        );

        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }

    public function getAllAuthors(): array
    {
        $stmt = $this->connection->prepare(
            'SELECT * FROM author'
        );
        $stmt->execute();
        $authors = $stmt->fetchAll();

        $authorsDto = [];
        foreach ($authors as $author) {
            $authorDto = new Author(
                $author['first_name'],
                $author['last_name'],
                $author['id'],
                $author['rating']);
            $authorsDto[] = $authorDto;
        }
        return $authorsDto;
    }

    private function deleteDataFromBooksAndAuthorsByBookId(string $id){
        $stmt = $this->connection->prepare(
            'DELETE FROM books_and_authors WHERE book_id = :bookId'
        );
        $stmt->bindValue(':bookId', $id);
        $stmt->execute();
    }

    private function addDataToBooksAndAuthors(array $authors, string $id) {
        $bookAuthorsStmt = $this->connection->prepare(
            'INSERT INTO books_and_authors (book_id, author_id) VALUES (:bookId, :authorId)'
        );

        foreach ($authors as $author) {
            $bookAuthorsStmt->bindValue(':bookId', intval($id));
            $bookAuthorsStmt->bindValue(':authorId', intval($author->id));

            $bookAuthorsStmt->execute();
        }
    }

    private function bindBookValues(PDOStatement $stmt, Book $book) {
        $stmt->bindValue(':bookTitle', $book->title);
        $stmt->bindValue(':rating', $book->grade);
        $stmt->bindValue(':isRead', $book->isRead);
    }

    private function bindAuthorValues(PDOStatement $stmt, Author $author) {
        $stmt->bindValue(':firstName', $author->firstName);
        $stmt->bindValue(':lastName', $author->lastName);
        $stmt->bindValue(':rating', $author->rating);
    }
}