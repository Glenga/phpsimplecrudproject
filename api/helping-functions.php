<?php

function validateBook(Book $book): array {
    $errors = [];

    if (strlen($book->title) < 3) {
        $errors[] = "Raamatu pealkiri on liiga lühike! Selle pikkus peab olema 3 kuni 23 märki";
    } elseif (strlen($book->title) > 23) {
        $errors[] = "Raamatu pealkiri on liiga pikk! Selle pikkus peab olema 3 kuni 23 märki";
    }

    return $errors;
}

function validateAuthor(Author $author):array {
    $errors = [];

    if (strlen($author->firstName) < 1) {
        $errors[] = "Autori eesnimi on liiga lühike! Selle pikkus peab olema 1 kuni 21 märki";
    } elseif (strlen($author->firstName) > 21) {
        $errors[] = "Autori eesnimi on liiga pikk! Selle pikkus peab olema 1 kuni 21 märki";
    }

    if (strlen($author->lastName) < 2) {
        $errors[] = "Autori perekonnanimi on liiga lühike! Selle pikkus peab olema 2 kuni 22 märki";
    } elseif (strlen($author->lastName) > 22) {
        $errors[] = "Autori perekonnanimi on liiga pikk! Selle pikkus peab olema 2 kuni 22 märki";
    }

    return $errors;
}

function printJson($object) {
    header("Content-Type: application/json");
    print json_encode($object, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
}