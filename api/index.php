<?php

require_once 'helping-functions.php';
require_once 'DaoDb.php';
require_once 'Book.php';
require_once 'Author.php';

$cmd = $_GET['cmd'] ?? "bookList";
$daoDb = new DaoDb();

if ($cmd === "bookList") {
    $books = $daoDb->getAllBooks();
    printJson($books);

} else if ($cmd === "findBookById") {
    $id = $_GET["id"];
    $book = $daoDb->getBookById($id);

    if ($book == null) {
        http_response_code(404);
    } else {
        printJson($book);
    }

} else if ($cmd === "addBook") {
    $json = file_get_contents("php://input");
    $bookData = json_decode($json, true);

    $book = Book::deserializeBook($bookData);
    $errors = validateBook($book);

    if (count($errors) > 0) {
        http_response_code(400);
        printJson(["errors" => $errors]);
    } else {
        $daoDb->addBook($book);
    }

} else if ($cmd === "editBook") {

    $json = file_get_contents("php://input");
    $bookData = json_decode($json, true);

    $book = Book::deserializeBook($bookData);
    $errors = validateBook($book);

    if (count($errors) > 0) {
        http_response_code(400);
        printJson(["errors" => $errors]);
    } else {
        $daoDb->updateBook($book);
        printJson($book);
    }

} else if ($cmd === "deleteBook") {

    $id = (intval($_GET["id"]));
    $daoDb->deleteBookById($id);
    http_response_code(204);

} else {
    http_response_code(400);
    printJson(["error" => "Unknown command : ${cmd}"]);
}