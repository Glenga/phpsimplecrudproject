<?php
const BOOKS_DATA_FILE = __DIR__ . "/project-resources/books.txt";
const AUTHORS_DATA_FILE = __DIR__ . "/project-resources/authors.txt";
const BOOKS_ID_FILE = __DIR__ . "/project-resources/books-id";
const AUTHORS_ID_FILE = __DIR__ . "/project-resources/authors-id.txt";