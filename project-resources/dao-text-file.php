<?php

function saveData(string $data, string $dataFileName) {

    $dataId = getDataId($data);
    if (findDataById($dataId, $dataFileName) != "") {

        $contents = '';
        foreach (getAllDataFromFile($dataFileName) as $dataLine) {

            $currentDataLineId = getDataId($dataLine);
            if ($dataId === $currentDataLineId) {
                $contents .= $data;
            } else {
                $contents .= $dataLine;
            }

        }
        file_put_contents($dataFileName, $contents);

    } else {
        file_put_contents($dataFileName, $data, FILE_APPEND);
    }
}

function findDataById(string $id, string $dataFileName) : string {

    foreach (getAllDataFromFile($dataFileName) as $dataLine) {
        if (getDataId($dataLine) === $id) {
            return $dataLine;
        }
    }

    return "";
}

function deleteDataById(string $id, string $dataFileName) {

    $contents = '';
    foreach (getAllDataFromFile($dataFileName) as $dataLine) {
        if (getDataId($dataLine) === $id) {
            continue;
        }

        $contents .= $dataLine;
    }
    file_put_contents($dataFileName, $contents);

}

function getAllDataFromFile(string $dataFileName): array {
    $lines = file($dataFileName);

    $result = [];

    foreach ($lines as $line) {
        $result[] = $line;
    }

    return $result;
}

function getNewId(string $idDataFileName) : string {
    $contents = file_get_contents($idDataFileName);

    $id = intval($contents);

    file_put_contents($idDataFileName, $id + 1);

    return strval($id);
}

function getDataId(string $dataLine) : string {
    $dataArray = explode(";", $dataLine);
    return str_replace(PHP_EOL, '',end($dataArray));
}