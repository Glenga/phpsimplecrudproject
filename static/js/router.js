import books from "./views/Books.js";
import bookForm from "./views/BookForm.js";

export const navigateTo = (path) => {
    history.pushState(null, null, path);
    router();
}

const removeAllChildNodes = (element) => {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

export const router = async () => {
    const routes = [
        { path: "/", renderView: books},
        { path: "/add-book", renderView: bookForm},
        { path: "/edit-book", renderView: bookForm},
        { path: "/authors" },
        { path: "/add-author" }
    ];
    const defaultRoute = routes[0];
    const currentPath = location.pathname;
    console.log(currentPath)
    const params = new URLSearchParams(location.search);

    const routeMatch = routes.find(route => route.path === currentPath);

    const bodyDiv = document.getElementById("body-div");
    removeAllChildNodes(bodyDiv);
    if (routeMatch) {
        const view = await routeMatch.renderView(params)
        bodyDiv.appendChild(view);
    } else {
        const view = await defaultRoute.renderView(params);
        bodyDiv.appendChild(view);
    }
}