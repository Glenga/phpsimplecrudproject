import {navigateTo} from "../router.js";

const bookForm = async (params) => {
    document.title = "Book form";

    return createBookForm(params)
}

async function createBookForm(params) {
    const formType = params.get("formType")

    const form = document.createElement("form")
    form.className = "form-grid"

    const titleLabel = document.createElement("label")
    titleLabel.for = "title"
    titleLabel.textContent = "Pealkiri: "
    const titleInput = document.createElement("input")
    titleInput.id = "title"
    titleInput.name = "title"
    titleInput.type = "text"

    const deleteButton = document.createElement("input")
    const submitButton = document.createElement("input")
    submitButton.className = "form-grid-button"
    submitButton.type = "button"
    submitButton.name = "submitButton"
    submitButton.value = "Salvesta"

    if (formType === "edit") {
        const id = params.get("id")
        const book = await fetch(`/api/index.php?cmd=findBookById&id=${id}`)
            .then(book => book.json())
        console.log(book)
        titleInput.value = book.title

        deleteButton.className = "form-grid-button"
        deleteButton.type = "button"
        deleteButton.name = "deleteButton"
        deleteButton.value = "Eemalda"
        deleteButton.addEventListener("click", event => {

            fetch(`/api/index.php?cmd=deleteBook&id=${id}`, {
                method: "POST"
            })
                .then(async response => {
                })
                .then(data => {
                    navigateTo("/");
                })
        })

        submitButton.addEventListener("click", event => {
            const title = document.getElementById("book-title").value;

            const update = {
                id,
                title
            }

            fetch("/api/index.php?cmd=editBook", {
                method: "POST",
                body: JSON.stringify(update)
            })
                .then(async response => {
                    return response
                })
                .then(data => {
                    navigateTo("/");
                })
        })

    } else {
        submitButton.addEventListener("click", event => {
            const title = document.getElementById("book-title").value;

            const update = {
                title
            }

            fetch("/api/index.php?cmd=addBook", {
                method: "POST",
                body: JSON.stringify(update)
            })
                .then(async response => {
                    return response
                })
                .then(data => {
                    navigateTo("/");
                })
        })
    }

    form.appendChild(titleLabel)
    form.appendChild(titleInput)
    if (formType === "edit") {
        form.appendChild(deleteButton)
    }
    form.appendChild(submitButton)

    return form
}

export default bookForm