const books = async (params) => {
    document.title = "Books";

    const books = await fetch("/api/index.php?cmd=bookList")
        .then(response => response.json());

    const table = document.createElement("div");
    table.className = "table-grid";

    const tableHeaders = [
         document.createElement("div"),
         document.createElement("div"),
         document.createElement("div")
    ]

    tableHeaders[0].textContent = "Pealkiri"
    tableHeaders[1].textContent = "Autorid"
    tableHeaders[2].textContent = "Hinne"
    tableHeaders[2].className = "able-grid-centered-item "

    tableHeaders.forEach(header => {
        header.className += "table-grid-header"
        table.appendChild(header)
    })

    books.forEach(book => {
        const bookName = document.createElement("div");
        bookName.className = "table-grid-item";
        const bookLink = document.createElement("a")
        bookLink.href = `/edit-book?formType=edit&id=${book.id}`
        bookLink.textContent = book.title
        bookName.appendChild(bookLink)

        const bookAuthors = document.createElement("div");
        bookAuthors.textContent = book.authors;
        bookAuthors.className = "table-grid-item";

        const bookGrade = document.createElement("div");
        bookGrade.textContent = book.grade;
        bookGrade.className = "table-grid-centered-item";

        table.appendChild(bookName)
        table.appendChild(bookAuthors)
        table.appendChild(bookGrade)
    })

    return table;
}

export default books;